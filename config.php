<?php 
	require 'vendor/autoload.php';
	require 'vendor/redbeanphp/rb.php';

// \Codebird\Codebird::setConsumerKey('OdukbOAcRSgUBjCDPhF2pn5Uq', 'bVHOxN1KxBjjDvNXaGe91skK9gnAEUfwIJS6AuYmpbZru44fB1'); // static, see 'Using multiple Codebird instances'
// $cb = \Codebird\Codebird::getInstance();

	// REDBEAN ORM CONFIG
	  R::setup("mysql:host=".DBHOST.";dbname=".DBNAME,
        DBUSER,DBPASSWORD); 

	 // slim microframework
	  $app = new \Slim\Slim(array(
	  	'mode'=>MODE,
		'view' => new \Slim\Views\Twig()
	));

	$view = $app->view();
	$view->parserOptions = array(
		'debug' => true,
		'cache' => false
		//'cache' => dirname(__FILE__) . '/cache'
		);

	$app->add(new \Slim\Middleware\SessionCookie(array(
	'expires' => '5 days',
	'path' => '/',
	'domain' => null,
	'secure' => false,
	'httponly' => false,
	'name' => 'slim_session',
	'secret' => 'CHANGE_ME',
	'cipher' => MCRYPT_RIJNDAEL_256,
	'cipher_mode' => MCRYPT_MODE_CBC
	)));

	$view->parserExtensions = array(
		new \Slim\Views\TwigExtension(),
		);

	$tmpl = $view->getEnvironment();

	// AUTO LOAD MODELS

	spl_autoload_register(function ($class){
		if(file_exists("models/{$class}.php"))
			include "models/{$class}.php";
	});

	/* mode */
	// Only invoked if mode is "production"
	$app->configureMode('production', function () use ($app) {
	    $app->config(array(
	        'log.enable' => true,
	        'debug' => false
	    ));
	});

	// Only invoked if mode is "development"
	$app->configureMode('development', function () use ($app) {
	    $app->config(array(
	        'log.enable' => false,
	        'debug' => true
	    ));
	});

	/* end  */

	$app->error(function (\Exception $e) use ($app) {
	   	$app->render('404.html');
	   	//echo '';
	});


 ?>
<?php 
		/* function */
function youtube_id_from_url($url) {
    $pattern = 
        '%^# Match any YouTube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char YouTube id.
        $%x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}


  function numberfloat($n)
{

    $number = floatval(str_replace(',', '',  $n));
    return $number;
}

  function addclosedeals($deals , $idreport){
   
      # code...
    
      
       R::exec( 'delete from closedeals where report_id = ?' , [$idreport]);


      for ($i=0; $i < count($deals['business_name']); $i++) { 
        # code...
         $dealr = R::dispense('closedeals');
         $dealr->report_id = $idreport;
         $dealr->business_name = $deals['business_name'][$i];
         $dealr->date_sold = $deals['date_sold'][$i];
         $dealr->list_price = (empty($deals['list_price'][$i])) ?  0 : numberfloat($deals['list_price'][$i]) ;
         $dealr->sold_price = (empty($deals['sold_price'][$i])) ? 0 : numberfloat($deals['sold_price'][$i]);
         $dealr->co_brokered = (empty($deals['co_brokered'][$i]))? 0 : numberfloat($deals['co_brokered'][$i]);
         $dealr->comm = (empty($deals['comm'][$i]))? 0 : numberfloat($deals['comm'][$i]);
         R::store($dealr);
      }
      return 0;
      
  }


   function addfranchisedetail($fran , $idreport , $typefran){

       R::exec( 'delete from franchisedetails where report_id = ? and type_franchise = ? '  , [$idreport , $typefran]);
      for ($i=0; $i < count($fran['business_name_fc']); $i++) { 
        # code...
         $dealr = R::dispense('franchisedetails');
         $dealr->report_id = $idreport;
         $dealr->name_business = $fran['business_name_fc'][$i];
         $dealr->comm = (empty($fran['comm_fc'][$i])) ? 0 : numberfloat($fran['comm_fc'][$i]);
         $dealr->type_franchise = $typefran;
         
         R::store($dealr);
      }
      return 0;
  }

     function addfranchisedetail2($fran , $idreport , $typefran){

       R::exec( 'delete from franchisedetails where report_id = ? and type_franchise = ? '  , [$idreport , $typefran]);
      for ($i=0; $i < count($fran['business_name_fcsold']); $i++) { 
        # code...
         $dealr = R::dispense('franchisedetails');
         $dealr->report_id = $idreport;
         $dealr->name_business = $fran['business_name_fcsold'][$i];
         $dealr->comm = (empty($fran['comm_fcsold'][$i])) ? 0 : numberfloat($fran['comm_fcsold'][$i]);
         $dealr->type_franchise = $typefran;
         
         R::store($dealr);
      }
      return 0;
  }

       function addfranchisedetail3($fran , $idreport , $typefran){

       R::exec( 'delete from franchisedetails where report_id = ? and type_franchise = ? '  , [$idreport , $typefran]);
      for ($i=0; $i < count($fran['business_name_fcdev']); $i++) { 
        # code...
         $dealr = R::dispense('franchisedetails');
         $dealr->report_id = $idreport;
         $dealr->name_business = $fran['business_name_fcdev'][$i];
         $dealr->comm = (empty($fran['comm_fcdev'][$i])) ? 0 : numberfloat($fran['comm_fcdev'][$i]);
         $dealr->type_franchise = $typefran;
         
         R::store($dealr);
      }
      return 0;
  }

 function slugify($text)
{ 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}
 ?>
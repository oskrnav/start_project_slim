<?php 
		function uploadImagec($data, $sizein = 500  , $sizeThumb= 200 , $dir = 'assets/uploads/'  , $CDN = FALSE){

			############ Edit settings ##############
			$ThumbSquareSize 		= $sizeThumb; //Thumbnail will be 200x200
			$BigImageMaxSize 		= $sizein; //Image Maximum height or width
			$ThumbPrefix			= "thumb_"; //Normal thumb Prefix
			$DestinationDirectory	= $dir;  //specify upload directory ends with / (slash)
			$Quality 				= 90; //jpeg quality
			##########################################
			
						
			// check $_FILES['ImageFile'] not empty
			if(!isset($data['ImageFile']) || !is_uploaded_file($data['ImageFile']['tmp_name']))
			{
					//die('Something wrong with uploaded file, something missing!'); // output error when above checks fail.
				return 0;
			}
			

			
			// Random number will be added after image name
			$RandomNumber 	= rand(0, 9999999999); 

			$ImageName 		= str_replace(' ','-',strtolower($data['ImageFile']['name'])); //get image name
			$ImageSize 		= $data['ImageFile']['size']; // get original image size
			$TempSrc	 	= $data['ImageFile']['tmp_name']; // Temp name of image file stored in PHP tmp folder
			$ImageType	 	= $data['ImageFile']['type']; //get file type, returns "image/png", image/jpeg, text/plain etc.

			//Let's check allowed $ImageType, we use PHP SWITCH statement here
			try {
				
		
			switch(strtolower($ImageType))
			{
				case 'image/png':
					//Create a new image from file 
					$CreatedImage =  imagecreatefrompng($data['ImageFile']['tmp_name']);
					break;
				case 'image/gif':
					$CreatedImage =  imagecreatefromgif($data['ImageFile']['tmp_name']);
					break;			
				case 'image/jpeg':
				case 'image/pjpeg':
					$CreatedImage = imagecreatefromjpeg($data['ImageFile']['tmp_name']);
					break;
				default:
				//	die('Unsupported File!'); //output error and exit
			}

				} catch (Exception $e) {
				
				//	die('invalid File');
					return 0;
				}
			
			//PHP getimagesize() function returns height/width from image file stored in PHP tmp folder.
			//Get first two values from image, width and height. 
			//list assign svalues to $CurWidth,$CurHeight
			list($CurWidth,$CurHeight)=getimagesize($TempSrc);
			
			//Get file extension from Image name, this will be added after random name
			$ImageExt = substr($ImageName, strrpos($ImageName, '.'));
		  	$ImageExt = str_replace('.','',$ImageExt);
			
			//remove extension from filename
			$ImageName 		= preg_replace("/\\.[^.\\s]{3,4}$/", "", $ImageName); 
			
			//Construct a new name with random number and extension.
			$NewImageName = $ImageName.'-'.$RandomNumber.'.'.$ImageExt;
			
			//set the Destination Image
			$thumb_DestRandImageName 	= $DestinationDirectory.$ThumbPrefix.$NewImageName; //Thumbnail name with destination directory
			$DestRandImageName 			= $DestinationDirectory.$NewImageName; // Image with destination directory
			
			//Resize image to Specified Size by calling resizeImage function.
			if(resizeImage($CurWidth,$CurHeight,$BigImageMaxSize,$DestRandImageName,$CreatedImage,$Quality,$ImageType))
			{
				//Create a square Thumbnail right after, this time we are using cropImage() function
				if(!cropImage($CurWidth,$CurHeight,$ThumbSquareSize,$thumb_DestRandImageName,$CreatedImage,$Quality,$ImageType))
					{
						//echo 'Error Creating thumbnail';
					}
				
				if ($CDN) {
						# code...
					include('s3_config.php');
					$tmp = $DestRandImageName;
					$bucket = 'toodolcdn';
					$actual_image_name = $NewImageName;

					if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PRIVATE) )
						{
							$s3->putObjectFile($thumb_DestRandImageName, $bucket , $ThumbPrefix.$NewImageName , S3::ACL_PRIVATE);
							$msg = "S3 Upload Successful.";	
							$s3file='https://deiz4i028y1jh.cloudfront.net/'.$actual_image_name;
							//echo "<img src='$s3file' style='max-width:400px'/><br/>";
							//echo '<b>S3 File URL:</b>'.$s3file;
						}
						return array('thumb'=>$ThumbPrefix.$NewImageName , 'image'=>$NewImageName , 's3file'=>$s3file );
				}
				else {

					return array('thumb'=>$ThumbPrefix.$NewImageName , 'image'=>$NewImageName  );
				}
				
				
				

			}else{
				//die('Resize Error'); //output error
			}
		}
			
	function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
	{	 
		//Check Image size is not 0
		if($CurWidth <= 0 || $CurHeight <= 0) 
		{
			return false;
		}
		
		//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
		if($CurWidth>$CurHeight)
		{
			$y_offset = 0;
			$x_offset = ($CurWidth - $CurHeight) / 2;
			$square_size 	= $CurWidth - ($x_offset * 2);
		}else{
			$x_offset = 0;
			$y_offset = ($CurHeight - $CurWidth) / 2;
			$square_size = $CurHeight - ($y_offset * 2);
		}
		
		$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
		if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
		{
			switch(strtolower($ImageType))
			{
				case 'image/png':
					imagepng($NewCanves,$DestFolder);
					break;
				case 'image/gif':
					imagegif($NewCanves,$DestFolder);
					break;			
				case 'image/jpeg':
				case 'image/pjpeg':
					imagejpeg($NewCanves,$DestFolder,$Quality);
					break;
				default:
					return false;
			}
		//Destroy image, frees memory	
		if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
		return true;

		}
		  
	}


	function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType)
	{
		//Check Image size is not 0
		if($CurWidth <= 0 || $CurHeight <= 0) 
		{
			return false;
		}
		
		//Construct a proportional size of new image

	$wRatio = $CurWidth / $MaxSize;
    $hRatio = $CurHeight / $MaxSize;
    
    $maxRatio = max($wRatio, $hRatio);

    if ($maxRatio > 1) {

    	$ImageScale      	= min($MaxSize/$CurWidth, $MaxSize/$CurHeight); 
		
		$NewWidth  			= ceil($ImageScale*$CurWidth);
		$NewHeight 			= ceil($ImageScale*$CurHeight);
       
    } else {
        $NewWidth = $CurWidth;
        $NewHeight = $CurHeight;
    }	


		


		$NewCanves 			= imagecreatetruecolor($NewWidth, $NewHeight);
		
		// Resize Image
		if(imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight))
		{
			switch(strtolower($ImageType))
			{
				case 'image/png':
					imagepng($NewCanves,$DestFolder);
					break;
				case 'image/gif':
					imagegif($NewCanves,$DestFolder);
					break;			
				case 'image/jpeg':
				case 'image/pjpeg':
					imagejpeg($NewCanves,$DestFolder,$Quality);
					break;
				default:
					return false;
			}
		//Destroy image, frees memory	
		if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
		return true;
		}

	}
 ?>